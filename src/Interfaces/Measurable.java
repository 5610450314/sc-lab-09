package Interfaces;

public interface Measurable {
	public double getMeasure();
}

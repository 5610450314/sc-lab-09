package Tree;

public class TraversalTest {
	public static void main(String[] args){
	Node n1 = new Node("A",null,null);
	Node n2 = new Node("C",null,null);
	Node n3 = new Node("E",null,null);
	Node n4 = new Node("H",null,null);
	Node n5 = new Node("D",n2,n3);
	Node n6 = new Node("I",n4,null);
	Node n7 = new Node("B",n1,n5);
	Node n8 = new Node("G",null,n6);
	Node n9 = new Node("F",n7,n8);
	ReportConsole rp = new ReportConsole();
	PreOrderTraversal pre = new PreOrderTraversal();
	PostOrderTraversal post = new PostOrderTraversal();
	InOrderTraversal in = new InOrderTraversal();
	System.out.print("pre-order :");
	rp.display(n9, pre);
	System.out.print("in-order :");
	rp.display(n9, in);
	System.out.print("post-order :");
	rp.display(n9, post);

	}
}

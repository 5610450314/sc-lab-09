package Model;

import java.util.ArrayList;

import Interfaces.Taxable;

public class TaxCalculator {

	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0;
		for(Taxable tax:taxList){
			sum += tax.getTax();
		}
		return sum;
	}
	
}

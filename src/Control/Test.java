package Control;


import java.util.ArrayList;
import java.util.Collections;

import Interfaces.Taxable;
import Model.Company;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxComparator;


public class Test {
	
	public static void TestPerson_part(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("FrydH",190,300000));
		persons.add(new Person("Ploum",180,280000));
		persons.add(new Person("iiamnut",150,100000));	
		Collections.sort(persons);
		System.out.println("Collects.sort(persons) :");
		for(Person p : persons){
			System.out.println(p);}
	}
	
	private static void TestProduct_part(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Perfume",500));
		products.add(new Product("Luis_Bag",5000));
		products.add(new Product("Swimming suit", 4500));	
		Collections.sort(products);
		System.out.println("Collects.sort(products) :");
		for(Product p : products){
			System.out.println(p);}
	}
	
	
	private static void TestCompany_part(){
		ArrayList<Company> companys = new ArrayList<Company>();
		companys.add(new Company("MAC_company",2000000,400000));
		companys.add(new Company("Luis_company",3000000,700000));
		companys.add(new Company("MCM_company",1000000,500000));
		Collections.sort(companys,new EarningComparator());
		
		System.out.println("Collects.sort(companys , EarningComparator) :");
		
		for(Company p : companys){
			System.out.println(p);}
		Collections.sort(companys,new ExpenseComparator());
		System.out.println("Collects.sort(companys , ExpenseComparator) :");
		for(Company p : companys){
			System.out.println(p);}
		Collections.sort(companys,new ProfitComparator());
		System.out.println("Collects.sort(companys , ProfitComparator) :");
		for(Company p : companys){
			System.out.println(p);}
	}

	public static void TestTax_part(){
		//person
				ArrayList<Taxable> taxs = new ArrayList<Taxable>();
				taxs.add(new Person("Ploum",150,300000));
				taxs.add(new Company("MAC_company",2000000,400000));
				taxs.add(new Product("Perfume",500));
				Collections.sort(taxs,new TaxComparator());
				System.out.println("Collects.sort(taxs , ProfitComparator) :");
				for(Taxable p : taxs){
					System.out.println(p.getTax());}
	}
	
	public static void main(String[] args)
	{
		System.out.println("Test :: 1");
		TestPerson_part();
		System.out.println("************************************************");
		
		System.out.println("Test :: 2");
		TestProduct_part();
		System.out.println("************************************************");
		
		System.out.println("Test :: 3");
		TestCompany_part();
		System.out.println("************************************************");
		
		System.out.println("Test :: 4");
		TestTax_part();
		
	}


}
